<?php // $Id$

/**
 * Implementation of hook_help().
 *
 * Throughout Drupal, hook_help() is used to display help text at the top of
 * pages. Some other parts of Drupal pages get explanatory text from these hooks
 * as well. We use it here to provide a description of the module on the
 * module administration page.
 */
function wghtml_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      // This description is shown in the listing at admin/modules.
      return t('wgHTML imports HTML pages to Drupal..');
    case 'node/add#wghtml':
      // This description shows up when users click "create content."
      return t('wgHTML content is created automagically. The results of manually creating a wgHTML node are unpredicatable - don\'t do it.');
  }
}

function wghtml_node_name($node) {
  return t('HTML page');
}

/**
 * Implementation of hook_access().
 *
 * Node modules may implement node_access() to determine the operations
 * users may perform on nodes. This example uses a very common access pattern.
 */
function wghtml_access($op, $node) {
  return true;
}

/**
 * Implementation of hook_load().
 *
 * Now that we've defined how to manage the node data in the database, we
 * need to tell Drupal how to get the node back out. This hook is called
 * every time a node is loaded, and allows us to do some loading of our own.
 */
function wghtml_load($node) {
  $page =& _wghtml_get_object();

  // see if the page needs loading
  if ( $page->pageId!==$node->nid ) {
    $page->get_page_from_id($node->nid);
  }
  // note we do not return a reference: there may be many nodes loaded with different pages
  return array('wghtmlPage' => $page);
}

/**
 * Implementation of hook_view().
 *
 * This is a typical implementation that simply runs the node text through
 * the output filters.
 */
function wghtml_view(&$node, $teaser = FALSE, $page = FALSE) {
  // substitute the cached body for the one stored in the node which is tag stripped
  // REVISIT - why not just store the HTML?
  $node->body = $node->wghtmlPage->body;
}

/**
 * A custom theme function.
 *
 * By using this function to format our node-specific information, themes
 * can override this presentation if they wish. We also wrap the default
 * presentation in a CSS class that is prefixed by the module name. This
 * way, style sheets can modify the output without requiring theme code.
 */
function theme_wgthml_view($node) {
  $output = '<div class="node_example_order_info">';
  $output .= t('The order is for %quantity %color items.', array('%quantity' => $node->quantity, '%color' => $node->color));
  $output .= '</div>';
  return $output;
}






/**
 * Action hook for show action
 */
function _wghtml_show($action=false, $file=false) {

  // get the wgHTML object which does all the work
  $page =& _wghtml_get_object();
  // get the requested page: $file is usually false so get_page uses the REQUEST_URI from the redirect as a default
  $page->get_page($file);

  // now load the node: the node load hook will add the page to the node
  $node = module_invoke('node', 'load', array('nid'=>$page->pageId));
  $output = module_invoke('node', 'view', $node);
  echo theme('page', $output);

return;

  if ( empty($page->error) ) {
    drupal_set_title($page->title);
    $output = $page->body;
    $output .= '<div style="font-size: 80%; border: solid black; border-width: 1px 0 0; text-align: right;">'.$page->status.'</div>';
  } else {
    $output  = '<div style="background: red; color: white; padding: 2px 4px;">Error: ';
    $output .= $page->error.'</div>';
  }
  if (true) {
    $output .= '<br /><br /><br />Dump of $page and $node for debug:<pre>';
    ob_start();
    print_r($page);
    print_r($node);
  	$output .= htmlspecialchars(ob_get_clean()) . '</pre>';
  }
  echo theme('page', $output);
}

