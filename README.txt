
COPYRIGHT AND LICENSE INFORMATION
=================================
wgHTML is copyright (c) WEBg8 2003-2005. djnz@webg8.com

wgHTML is licensed under the GNU General Public License (GPL): see
http://www.opensource.org/licenses/gpl-license.php


REQUIREMENTS
============
1. Only tested with Drupal v 4.6.3 but should work with some other versions:
   give it a try! Please report back to the forums at http://www.webg8.com/
   so that issues can be resolved for everybody.
2. Apache with mod_rewrite.
3. If you do not have clean url's enabled in Drupal, change the references in
   section 4 of the instructions from /wghtml/show to index.php?q=wghtml/show


INSTALLATION
============
1. Copy the files to the appropriate directory

2. Create the following tables using phpMyAdmin or whatever:
CREATE TABLE `wghtml_pages` (
  `pageId` int(8) unsigned NOT NULL auto_increment,
  `identity` varchar(32) NOT NULL default '',
  `current` int(8) NOT NULL default '0',
  PRIMARY KEY  (`pageId`),
  KEY `identity` (`identity`)
);
CREATE TABLE `wghtml_versions` (
  `pageId` int(8) unsigned NOT NULL default '0',
  `version` int(8) NOT NULL default '0',
  `node` int(8) unsigned NOT NULL default '0',
  `perm_area` varchar(32) default NULL,
  `raw` text,
  `title` text,
  `head` text,
  `body` text,
  `bodyattrib` text,
  `linkto` text,
  `signature` int(10) unsigned default NULL,
  `cachetime` int(10) unsigned default NULL,
  PRIMARY KEY  (`pageId`,`version`)
)

3. Review the options in the file wghtml/config.php. The most significant one
is the 'implementation' setting. Decide whether you want to run wgHTML as a
stand-alone module (simpler, less to go wrong but no comments), or as a node
module (full integration so comments, taxonomy, search etc. work as for any
node).

4. The final stage depends on where your HTML files and your Drupal files are
located: choose the relevant option below.

4a. Drupal and HTML files in separate directories (say
http://www.mysite.com/drupal/ and http://www.mysite.com/htmlfiles/)

Put the following in htmlfiles/.htaccess

RewriteEngine On
RewriteCond %{REQUEST_URI} \.php$|\.html?$|\.txt$|\.doc$|\.me$ [NC]
RewriteRule ^(.*)$ /drupal/wghtml/show [L]

4b. Drupal in the web root directory, HTML files in a subdirectory ( say
/htmlfiles/)

Put the following in htmlfiles/.htaccess

RewriteEngine On
RewriteCond %{REQUEST_URI} \.php$|\.html?$|\.txt$|\.doc$|\.me$ [NC]
RewriteRule ^(.*)$ /wghtml/show [L]

4c. Drupal in a subdirectory (/drupal/), HTML files in the web root

Put the following in .htaccess in the web root directory

RewriteEngine On
RewriteCond %{REQUEST_URI} !^\/drupal\/
RewriteCond %{REQUEST_URI} \.php$|\.html?$|\.txt$|\.doc$|\.me$ [NC]
RewriteRule ^(.*)$ /drupal/wghtml/show [L]

4d. Drupal and HTML files in the web root

How on earth do you maintatin that? Anyway, put the following in .htaccess
in the web root directory

RewriteEngine On
RewriteCond %{REQUEST_URI} \.html?$ [NC]
RewriteRule ^(.*)$ /wghtml/show [L]

Note that this is not ideal, and will mean that files with extensions other than 
.html (or htm) will not display within Drupal, and any .html (or .htm) files
within Drupal itself will be displayed within wgHTML. Mind you, this might be 
desirable behaviour.

5. Visit some HTML files: they should be displayed as Drupal nodes. Drupal
places nodes in the search index automatically, but only when cron.php is
run so you might like to load cron.php manually to check you can search
for some words in your files.

6. Feed back any problems you have had to the forum at http://www.webg8.com/
so that improvements can be made to the code or documentation.


TO DO LIST
==========
Change direct references to the wghtml_config global to $this->config
Review the rewriting of links when parsing source
Infer index.htm[l] for missing file names
Deal with nonexistant source files
Deal with broken source files (did exist once but don't now)
When retrieving a page from a node id, the source file is not currently
  checked - it should be (see get_page_from_id)

Add more options for checking/not checking source
Add configurable options for adding nodes (timestamp, user id)
Implement permission areas
Create watchdog entries on caching a page
Improve handling of text and other source files
Review HTML parsing so it is more robust
Add theming to drupal node implementation
Add theming to module implementation
Add directory crawling initiated by admin
Add web crawling initiated by admin
Add option for a 'one time' input so HTML is subsequently maintained
  purely as a node
Optional revision control (store/access previous versions)
Site map

