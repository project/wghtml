<?php // $Id$
/*
 * Implementation of wgHTML as a module for Drupal
 * Tested with Drupal 4.6.3
 */
class wghtml_implementation extends wghtml_page {

  function wghtml_implementation() {
    // call the parent constructor
    parent::wghtml_page();
  }

/*
 * Retrieve a page from the cache
 */
  function retrieve_page() {
    $ret = db_fetch_object(db_query('SELECT pageId FROM {wghtml_pages} WHERE identity = \'%s\'', $this->identity));
    $ret = db_fetch_object(db_query('SELECT * FROM {wghtml_versions} WHERE pageId = %d ORDER BY version DESC LIMIT 1', $ret->pageId));
    if ( $ret ) {
      foreach ( $ret as $key=>$value ) {
        $this->$key = $value;
      }
      return true;
    } else {
      return false;
    }
  }

/*
 * Check access permission for a permission area
 *
 */
  function check_perm($perm_area=FALSE) {
    return TRUE;
  }

/*
 * Deal with a page that is in the cache but does not exist
 *
 */
  function save_page_broken($identity=false) {
    return;
  }

/*
 * Save a page to the cache
 */
  function save_page() {

//      variable_set('wghtml_options', array('uid'=>99));
//      $this->options = variable_get('wghtml_options', array());

    if ( empty($this->version) ) {

      // new page
      $this->version = 1;
      db_query('LOCK TABLES {wghtml_versions} WRITE');
      // TODO should check here that it has still not been created and return if it has

      // get the next id
      $this->pageId = db_next_id('wghtml_pageId');
      // create page record
      db_query('INSERT INTO {wghtml_pages} (
        pageId, identity )
        VALUES (%d, \'%s\')',
        $this->pageId, $this->identity, 1);

    } else {
      // new version of existing page: get maximum version
      db_query('LOCK TABLES {wghtml_versions} WRITE');
      $max = db_result(db_query('SELECT version FROM {wghtml_versions} WHERE pageId = %d ORDER BY version DESC LIMIT 1', $this->pageId));
      $this->version = $max + 1;

    }

    // save version
    db_query("INSERT INTO {wghtml_versions} (
      pageId, version, node, perm_area, raw, title, head, body, bodyattrib, linkto, signature, cachetime)
      VALUES ('%s', %d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d, %d)",
      $this->pageId, $this->version, 0, $this->perm_area, $this->raw, $this->title, $this->head, $this->body, $this->bodyattrib, $this->linkto, $this->signature, time());
    db_query('UNLOCK TABLES');

    // update search index
    search_index($this->pageId, 'wghtml', $this->raw);

    // TODO get this watchdog entry correct
    // watchdog('content', t('wghtml added %title.', array('%title' => $this->title)), WATCHDOG_NOTICE, l(t('view'), "/"));

    return true;

  }
}
