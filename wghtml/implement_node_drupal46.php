<?php // $Id$
/*
 * Implementation of wgHTML as a node for Drupal
 * Tested with Drupal 4.6.3
 */
class wghtml_implementation extends wghtml_page {

  function wghtml_implementation() {
    // call the parent constructor
    parent::wghtml_page();
  }

/*
 * Check access permission for a permission area
 *
 */
  function check_perm($perm_area=FALSE) {
    return TRUE;
  }

/*
 * Deal with a page that is in the cache but does not exist
 *
 */
  function save_page_broken($identity=false) {
    return;
  }

/*
 * Retrieve a page from the cache
 */
  function retrieve_page() {
    // REVISIT - surely could get one query here?
    $ret = db_fetch_object(db_query('SELECT pageId FROM {wghtml_pages} WHERE identity = \'%s\'', $this->identity));
    $ret = db_fetch_object(db_query('SELECT * FROM {wghtml_versions} WHERE pageId = %d ORDER BY version DESC LIMIT 1', $ret->pageId));
    if ( $ret ) {
      foreach ( $ret as $key=>$value ) {
        $this->$key = $value;
      }
      return true;
    } else {
      return false;
    }
  }

/*
 * If we are attaching the page to a node, we need to be able to load the page
 * without knowing its location, so we need to use the page id - which is
 * fortunately the same as the node id
 */
  function retrieve_page_from_id($id) {
    $ret = db_fetch_object(db_query('SELECT * FROM {wghtml_versions} WHERE pageId = %d ORDER BY version DESC LIMIT 1', $id));
    if ( $ret ) {
      foreach ( $ret as $key=>$value ) {
        $this->$key = $value;
      }
      return true;
    } else {
      return false;
    }
  }


/*
 * Save the page into the cache. As we want the page to be a Drupal node,
 * we need to keep the node synched with the page using the node id as the pageId
 */
  function save_page() {

    if ( empty($this->version) ) {

      // new page
      $this->version = 1;

      // create a new node
      $node = $this->make_node();
      $this->pageId = node_save($node);

      // TODO why doesn't this work? db_query('LOCK TABLES {wghtml_versions} WRITE');
      // TODO should check here that it has still not been created and return if it has

      // create page record
      db_query('INSERT INTO {wghtml_pages} (
        pageId, identity )
        VALUES (%d, \'%s\')',
        $this->pageId, $this->identity, 1);

    } else {

      // new version of existing page
      $node = $this->make_node();
      node_save($node);

      // get maximum version number
      db_query('LOCK TABLES {wghtml_versions} WRITE');
      $max = db_result(db_query('SELECT version FROM {wghtml_versions} WHERE pageId = %d ORDER BY version DESC LIMIT 1', $this->pageId));
      $this->version = $max + 1;

    }

    // save version
    db_query("INSERT INTO {wghtml_versions} (
      pageId, version, node, perm_area, raw, title, head, body, bodyattrib, linkto, signature, cachetime)
      VALUES ('%s', %d, %d, '%s', '%s', '%s', '%s', '%s', '%s', '%s', %d, %d)",
      $this->pageId, $this->version, 0, $this->perm_area, $this->raw, $this->title, $this->head, $this->body, $this->bodyattrib, $this->linkto, $this->signature, time());
    db_query('UNLOCK TABLES');

    // We do NOT need to update the search index as cron will do this automagically
    // for nodes
    // search_index($this->pageId, 'wghtml', $this->raw);

    // validate node and get any errors
    // TODO get this watchdog entry correct
    // watchdog('content', t('wghtml added %title.', array('%title' => $this->title)), WATCHDOG_NOTICE, l(t('view'), "/"));

    return true;

  }

/*
 * Create a node-like object from the page details
 */
  function make_node() {
    $node = (object)array();
    if ( !empty($this->pageId) ) {
      $node->nid = $this->pageId;
    }
    $node->uid = $GLOBALS['wghtml_config']['userid']; // not sure what happens if there is no user id
    $node->type = 'wghtml';
    $node->title = $this->title;
    // $node->teaser = TODO
    $node->body = strip_tags($this->body);
    $node->created = $this->signature;
    $node->changed = $this->signature;
    $node->revisions = '';
    $node->comment = 2;
    $node->format = 1;
    //  'teaser', 'revisions', 'status', 'promote', 'moderate', 'sticky', 'format');
    return $node;
  }

}
