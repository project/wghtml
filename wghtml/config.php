<?php // $Id$

global $wghtml_config;

$wghtml_config = array(

// IMPLEMENTATION ------------------------------------------
// 'implementation' => 'module_drupal46',
'implementation' => 'node_drupal46',

// USER ID FOR NEW PAGE RECORDS ----------------------------
'userid' => 1,

// REFRESH INTERVAL
'staletime' => 60*60*24*300, // refresh every 300 days

);
