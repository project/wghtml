<?php // $Id$

class wghtml_page {

  function wghtml_page() {
    $this->config =& $GLOBALS['wgthml_config'];
  }

/**
 * Get a canonical identity from a path.
 *
 * For short paths, the identity is just the path. Long paths are hashed so
 * the identity will always fit in 32 characters. \'s in the path are
 * changed to /'s so equivalent paths should always have equivalent
 * identities (/../ in the path is not supported)
 *
 * @param $path
 *   A string containing a path.
 * @return
 *   The equivalent (32 char max) identity
 */
  function get_identity($path) {
    $path = str_replace('\\', '/', $path);
    if ( strlen($path)>32 ) {
      return md5($path);
    } else {
      return $path;
    }
  }

/**
 * Get a page with a given id.
 *
 * For short paths, the identity is just the path. Long paths are hashed so
 * the identity will always fit in 32 characters. \'s in the path are
 * changed to /'s so equivalent paths should always have equivalent
 * identities (/../ in the path is not supported)
 *
 * @param $path
 *   The numeric page id.
 * @return
 *   Null. The page is loaded into the current object.
 */
  function get_page_from_id($pageId) {
    // TODO this just retrieves the cached page: we should do the same checks as get_page
    // on the status of the source file. We therefore need the full source file location,
    // not just the hashed identity
    $this->retrieve_page_from_id($pageId);
  }

/**
 * Get a page from the cache or from its source as appropriate
 *
 * @param $decoded_url
 *   Optionally supply a path (no host) to a file. Normally this will not be
 *   supplied and the originally requested file before the redirect will
 *   be used
 * @param $bypass_perm
 *   Optionally (set to true) bypass the user's permission for the file
 * @param $cache
 *   Optionally change the caching behaviour - see below
 * @return
 *   Null. The page is loaded into the current object.
 *
 * caching options:
 *   true      - (default) refresh if changed or stale
 *   false     - get straight from the source ignoring the cache
 *   'refresh' - force a refresh - NOT IMPLEMENTED
 *   'nocheck' - fetch from the cache unless it is stale - NOT IMPLEMENTED
 *   'nostale' - fetch from the cache unless changed even if stale - NOT IMPLEMENTED
 *   'always'  - fetch from the cache ignoring the source - NOT IMPLEMENTED
 */
  function get_page($decoded_url=false, $bypass_perm=false, $cache=true) {

  if ( !$decoded_url ) {
      $decoded_url = urldecode($_SERVER['REQUEST_URI']);
    }

    $this->identity = $this->get_identity($decoded_url);
    $this->perm_area = $this->get_identity(dirname($decoded_url));

    // first check view permission
    if ( !($bypass_perm || $this->check_perm($this->perm_area)) ) {
      $this->error = 'noperm';
      return;
    }

    if ( $cache ) {
      if ( $this->retrieve_page($this) ) {

        // page exists in cache, but does it still exist on disk?
        if ( !file_exists($_SERVER['DOCUMENT_ROOT'].$decoded_url) ) {
          $this->save_page_broken($this->pageVerId);
          $this->error = 'unexists';
          return;
        }

        // or has it been modified?
        $signature = filemtime($_SERVER['DOCUMENT_ROOT'].$decoded_url);
        if ( $this->signature!=$signature ) {
          // cache needs refreshing
          $status = 'updated';

        // or is it stale?
        } elseif ( ($this->cachetime + $this->config['staletime']) < time() ) {
          // cache needs refreshing
          $status = 'refreshed';

        // none of the above, so continue with retrieved page
        } else {
          $status = 'retrieved';
          $retrieved = TRUE;
        }

      } else {
        // page was not in cache
        $status = 'fetched';
      }

      if ( empty($retrieved) ) {
        $this->raw = $this->get_contents($decoded_url);
        if ( $this->raw===false ) {
          $this->save_page_broken($this); // if it was retrieved but refresh showed it broken
          $this->error = 'unexists';
          return;
        }
        $this->parse_html($this->rebase_urls($this->raw));
        $this->linkto = $decoded_url;
        $this->signature = filemtime($_SERVER['DOCUMENT_ROOT'].$decoded_url);
        $this->status = $status;
        $this->save_page($this);
      }

    } else {
      //  get straight from the source ignoring the cache
      $this->raw = $this->get_contents($decoded_url);
      if ( $this->raw===false ) {
        $this->error = 'unexists';
        return;
      }
      $this->parse_html($this->rebase_urls($this->raw));
      $this->linkto = $decoded_url;
      $status = 'fetched';
    }

    $this->status = $status;

    return;

  } // end function get_page

/**
 * Rebase relative url's in HTML code
 *
 * If the code is going to be displayed on a page using <base href="...">,
 * relative links in the original code will not work. This code will add the
 * path to the original file to references to make them absolute.
 *
 * @param $text
 *   A string containing the HTML to be parsed.
 * @return
 *   The parsed HTML.
 */
  function rebase_urls($text) {
    $find = array(
      '@(<a\\s[^>]*href\\s*=\\s*")([^/\\\\][^":]*")@Umsi', // <a href=...
      '@(<img\\s[^>]*src\\s*=\\s*")([^/\\\\][^":]*")@Umsi', // <img src=...
      );
    // insert web path to current page
    $repl = '\\1'.$this->fileinfo['webdir'].'/\\2';
    $text =  preg_replace($find, $repl, $text);
    return $text;
  }

/**
 * Get the contents of a file
 *
 * For HTML files this is easy: other files need special treatment as they
 * will be displayed within HTML so you cannot send them as eg text
 *
 * @param $fname
 *   Optional file name (and path). If not supplied, the originally requested
 *   file (before the redirect) is used.
 * @return
 *   The contents of the file as HTML.
 */
  function get_contents($fname=FALSE) {

    if ( empty($fname) ) {
      $fname = $_SERVER['REQUEST_URI'];
    }

    $file = pathinfo($_SERVER['DOCUMENT_ROOT'] . $fname);  // dirname, basename, extension
    !isset($file['extension']) && $file['extension'] = '';
    $file['webpath'] = $fname;
    $file['webdir'] = dirname($fname);
    $file['fullpath'] = $_SERVER['DOCUMENT_ROOT'] . $fname;
    $this->fileinfo = $file;

    if ( !file_exists($file['fullpath']) ) {
      return false;
    } elseif ( substr($file['extension'], 0, 3)=='htm' ) {
      $content = file_get_contents($file['fullpath']); // TODO cache this
    } elseif ( $file['extension']=='php' ) {
      /* needs fixing
      $webg8->wrap['app_wrapper']->pre_include($webg8); // some apps require this before output buffering
      ob_start();
      // NOW INCLUDE THE ORIGINALLY REQUESTED FILE
      include $file['fullpath'];
      $content = ob_get_clean();
      */
    } else {
      // TODO text files should be dealt with some other way
      $content = "<html>\n<head>\n</head>\n<body>\n<pre>\n";
      $content .= htmlspecialchars(file_get_contents($file['fullpath']));
      $content .= "\n</pre>\n</body>\n</html>\n";
    }
    return $content;
 
  } // end function get_contents


/**
 * Parse HTML into sections
 *
 * Split HTML into head, title, body and body attributes, saving them to the
 * current object
 *
 * @param $content
 *   The HTML to parse
 * @return
 *   Null. $this->doctype, $this->head, $this->title, $this->body and
 *   $this->bodyattr are set appropriately.
 */
  function parse_html($content) {

    // DOC TYPE DEFINITION
    $regex = '/(.*)(<html.*>)/Umsi';
    preg_match($regex, $content, $matches);
    // if there is no html but there is a document type definition we should never have
    // touched it - it could be xml or something
    if ( empty($matches[2]) ) {
      $regex = '/<\?xml/Umsi';
      preg_match($regex, $content, $matches);
      if ( !empty($matches[0]) ) {
        echo $content;
        die;
        // there should be a more graceful way of dealing with it but this will do for now
      }
    }
    // set doctype if there is one
    isset($matches[1]) && $this->doctype = $matches[1];

    // anything in the <head>, including the title
    $regex = '@<head.*>(.*)</head\\s*>@Umsi';
    preg_match($regex, $content, $matches);
    if ( isset($matches[1]) ) {
      // find the title
      $this->head = $matches[1];
      $regex = '@(<title.*>)(.*)(</title\\s*>)@Umsi';
      preg_match($regex, $matches[1], $matches);
      if ( isset($matches[2]) ) {
        $this->title = trim($matches[2]);
        $this->head = trim(str_replace($matches[0], '', $this->head));
      }
    }
    if ( isset($this->head) && trim($this->head)=='' ) {
      unset($this->head);
    }

    // if there is no body, take the lot
    $regex = '@<body(.*)>(.*)</body\\s*>@Umsi'; // body tag attributes and the body
    preg_match($regex, $content, $matches);
    if ( isset($matches[1]) && trim($matches[1]) ) {
      // <body> attributes to use in a wrapping <div> or whatever
      $this->bodyattr = trim($matches[1]);
    }
    if ( isset($matches[2]) ) {
      $this->body = trim($matches[2]);
    } else {
      $this->body = trim($content);
    }

    return $page;

  } // end function parse_html

} // end class wghtml_page
