<?php // $Id$

/**
 * Implementation of hook_help().
 */
function wghtml_help($section) {
  switch ($section) {
    case 'admin/modules#description':
      // This description is shown in the listing at admin/modules.
      return t('Import HTML pages to nodes.');
      // REVISIT could do with more help here perhaps
  }
}

/**
 * Implementation of hook_search().
 */
function wghtml_search($op = 'search', $keys = null) {
  switch ($op) {
    case 'name':
      return variable_get('wghtml_search_tab', 'HTML Pages');
    case 'reset':
      variable_del('wghtml_cron_last_search');
      return;
    case 'search':
      $find = do_search($keys, 'wghtml', 'INNER JOIN {wghtml_pages} p ON p.pageId = i.sid');
      $results = array();
      foreach ($find as $item) {
        $page = wghtml_page::retrieve_page_from_id($item);
        $results[] = array('link' => $page['linkto'],
                           'type' => variable_get('wghtml_search_result', ''),
                           'type' => $page['linkto'],
                           'title' => $page['title'] ? $page['title'] : '(No title)',
                           'date' => $page['signature'],
                           // what is this ? 'extra' => '',
                           'snippet' => search_excerpt($keys, strip_tags($page['title']."\n".$page['body'])));
      }
      return $results;
  }
}


// Private functions ----------------------------------------------------------

/**
 * Action hook for show action
 */
function _wghtml_show($action=false, $file=false) {
  require_once dirname(__FILE__).'/wghtml/class_wghtml.php';
  require_once dirname(__FILE__).'/wghtml/implement_'.$GLOBALS['wghtml_config']['implementation'].'.php';
  $page =& new wghtml_implementation;
  $page->get_page($file);
  if ( empty($page->error) ) {
    drupal_set_title($page->title);
    $output = $page->body;
    $output .= '<div style="font-size: 80%; border: solid black; border-width: 1px 0 0; text-align: right;">'.$page->status.'</div>';
  } else {
    $output  = '<div style="background: red; color: white; padding: 2px 4px;">Error: ';
    $output .= $page->error.'</div>';
  }
  if (false) {
    $output .= '<br /><br /><br />Dump of $page for debug:<pre>';
  	ob_start();
    print_r($page);
  	$output .= htmlspecialchars(ob_get_clean()) . '</pre>';
  }
  echo theme('page', $output);
}

/**
 * Action hook for home
 */
function _wghtml_home() {
  _wghtml_show('show', '/hpages/index.html');
}

